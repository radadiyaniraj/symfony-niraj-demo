<?php
/**
 * Created by PhpStorm.
 * User: kapil.patel
 * Date: 6/7/2018
 * Time: 4:37 PM
 */

namespace AppBundle\Service;

class Generator
{
    private $generators;

    public function addGenerator(GeneratorInterface $generator){
        $this->generators[] = $generator;
        return $this->generators;
    }

    public function generate($name){

        foreach ($this->generators as $generator) {
            if ($generator->supports($name)) {
                return $generator->generate();
            }
        }

        throw new \RuntimeException('No supported Converters found in chain.');
    }
}