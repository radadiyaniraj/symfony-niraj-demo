<?php
namespace AppBundle\Service;

class UserPaymentsReportGenerator implements GeneratorInterface
{
    public function supports($name)
    {
        return $name === 'user_payments';
    }
    public function generate()
    {
        $message = "User Payments Parser";
        return $message;
    }
}