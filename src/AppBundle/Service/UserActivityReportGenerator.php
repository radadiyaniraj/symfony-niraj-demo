<?php
namespace AppBundle\Service;

class UserActivityReportGenerator implements GeneratorInterface
{

    /**
     * @param $name
     * @return bool
     */
    public function supports($name)
    {
        return $name === 'user_activity';
    }


    public function generate()
    {
        $message = "User Activity Generator";
        return $message;
    }
}