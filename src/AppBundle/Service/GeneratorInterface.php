<?php
namespace AppBundle\Service;

interface GeneratorInterface
{
    public function generate();
    public function supports($name);

}
