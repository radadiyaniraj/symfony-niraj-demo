<?php

namespace AppBundle\DependencyInjection\Compiler;

use AppBundle\Service\UserPaymentsReportGenerator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

//use AppBundle\Service\UserActivityReportGenerator;
//use AppBundle\Service\UserPaymentsReportGenerator;

class GeneratorPass implements CompilerPassInterface
{
    const SERVICE_ID = 'generator';
    const TAG_ID = 'app.generator';

    public function process(ContainerBuilder $container)
    {
        if (!$container->has(self::SERVICE_ID)) {
            return false;
        }

        $definition = $container->findDefinition(self::SERVICE_ID);

        // find all the services that are tagged as app.generator
        $taggedServices = $container->findTaggedServiceIds(self::TAG_ID);

        foreach ($taggedServices as $id => $tag) {
            // add the service to the Service\Generator::$generators array
            $definition->addMethodCall(
                'addGenerator',
                [
                    new Reference($id)
                ]
            );
        }
    }
}