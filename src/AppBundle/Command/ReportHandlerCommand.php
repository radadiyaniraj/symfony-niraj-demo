<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ReportHandlerCommand
 * @package AppBundle\Command
 */
class ReportHandlerCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('report:handler')
            ->setDescription('Generates reports about user activity, payments, etc.')
            ->addArgument('report_generator_name', InputArgument::REQUIRED, 'Report Generator Name')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('report_generator_name');
        $result = $this->getContainer()->get('generator')->generate($argument);
        $output->writeln($result);
    }
}
